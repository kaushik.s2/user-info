package com.example.userinfo;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private List<User> userListInfo = new ArrayList<>();


    public UserAdapter(List<User> userListInfo) {
        this.userListInfo = userListInfo;
    }

    @NonNull
    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_demo, parent, false);
        return new ViewHolder(view);

    }


    @Override
    public void onBindViewHolder(@NonNull UserAdapter.ViewHolder holder, int position) {
        final User user = userListInfo.get(position);
        holder.text_user1.setText(user.getName());
        holder.text_number1.setText(user.getNumber() + "");


    }

    @Override
    public int getItemCount() {
        return userListInfo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView text_user1;
        public TextView text_number1;

        public ViewHolder(View view) {
            super(view);
            this.text_user1 = (TextView) text_user1.findViewById(R.id.text_user1);
            this.text_number1 = (TextView) text_number1.findViewById(R.id.text_number1);

        }
    }
}
